package com.edataconsulting.prueba;

import com.edataconsulting.prueba.controller.MsgController;
import com.edataconsulting.prueba.controller.PermController;
import com.edataconsulting.prueba.controller.UsersController;
import com.edataconsulting.prueba.repository.MsgRepository;
import com.edataconsulting.prueba.repository.PermRepository;
import com.edataconsulting.prueba.repository.UsersRepository;
import com.edataconsulting.prueba.service.MsgService;
import com.edataconsulting.prueba.service.PermService;
import com.edataconsulting.prueba.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PruebaApplicationTests {
	@Autowired
	MsgController msgController;
	@Autowired
	MsgService msgService;
	@Autowired
	MsgRepository msgRepository;
	@Autowired
	PermService permService;
	@Autowired
	PermController permController;
	@Autowired
	PermRepository permRepository;
	@Autowired
	UsersRepository usersRepository;
	@Autowired
	UsersService usersService;
	@Autowired
	UsersController usersController;

	@Test
	void contextLoads() {
		assertThat(msgController).isNotNull();
		assertThat(msgService).isNotNull();
		assertThat(msgRepository).isNotNull();
		assertThat(usersController).isNotNull();
		assertThat(usersService).isNotNull();
		assertThat(usersRepository).isNotNull();
		assertThat(permController).isNotNull();
		assertThat(permService).isNotNull();
		assertThat(permRepository).isNotNull();

	}


}
