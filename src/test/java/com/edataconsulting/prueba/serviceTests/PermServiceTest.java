package com.edataconsulting.prueba.serviceTests;

import com.edataconsulting.prueba.entity.Perm;
import com.edataconsulting.prueba.repository.PermRepository;
import com.edataconsulting.prueba.service.PermService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;


@SpringBootTest
public class PermServiceTest {
    private PermService permService;

    @MockBean
    private PermRepository permRepository;

    @BeforeEach
    void setUp() {
        permService = new PermService(permRepository);
    }
    @Test
    void whenCallFindAll_shouldCallPermRepositoryFindAllAndReturnResult() {
        List<Perm> perm_list = new ArrayList<>();
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");
        perm_list.add(perm);

        when(permRepository.findAll()).thenReturn(perm_list);

        List<Perm> result = permService.findAll();

        verify(permRepository).findAll();
        assertThat(result).isEqualTo(perm_list);
    }
    @Test
    void whenCallFindById_shouldCallPermRepositoryFindByIdAndReturnPerm() {
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");

        when(permRepository.findById(1l)).thenReturn(Optional.of(perm));

        Perm result = permService.findById(1L);

        verify(permRepository).findById(1L);
        assertThat(result).isEqualTo(perm);

    }
    @Test
    void whenCallAdd_shouldCallPermRepositoryAddAndReturnNewPerm() {
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");

        when(permRepository.save(perm)).thenReturn(perm);

        Perm result = permService.add(perm);

        verify(permRepository).save(perm);
        assertThat(result).isEqualTo(perm);
    }
    @Test
    void whenCallUpdate_shouldCallMessageRepositoryUpdateAndReturnNewMessage() {
        Perm orig_perm = new Perm();
        orig_perm.setPerm_type("PERM1");

        Perm new_perm = new Perm();
        new_perm.setPerm_type("PERM2");

        Perm upd_perm = orig_perm;
        upd_perm.setPerm_type("PERM2");

        when(permRepository.findById((long) 1)).thenReturn(Optional.of(orig_perm));
        when(permRepository.save(any(Perm.class))).thenReturn(upd_perm);

        Perm result = permService.update(1, new_perm);

        verify(permRepository).findById((long) 1);
        verify(permRepository).save(upd_perm);
        assertThat(result).isEqualTo(upd_perm);
    }
   @Test
    void whenCallDelete_shouldCallMessageRepositoryDeleteAndDeletePerm() {
        permService.delete(1L);
        verify(permRepository).deleteById(any(Long.class));
    }


}
