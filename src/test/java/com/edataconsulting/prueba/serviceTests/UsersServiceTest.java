package com.edataconsulting.prueba.serviceTests;

import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.repository.UsersRepository;
import com.edataconsulting.prueba.service.UsersService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UsersServiceTest {
    private UsersService usersService;

    @MockBean
    private UsersRepository usersRepository;

    @BeforeEach
    void setUp() {usersService = new UsersService(usersRepository);}
    @Test
    void whenCallFindAll_shouldCallUsersRepositoryFindAllAndReturnResult() {
        List<Users> user_list = new ArrayList<>();
        Users user = new Users();
        user.setU_name("USERMAME");
        user_list.add(user);

        when(usersRepository.findAll()).thenReturn(user_list);

        List<Users> result = usersService.findAll();

        verify(usersRepository).findAll();
        assertThat(result).isEqualTo(user_list);
    }
    @Test
    void whenCallFindById_shouldCallUsersRepositoryFindByIdAndReturnUsers() {
        Users user = new Users();
        user.setU_name("USERNAME");

        when(usersRepository.findById(1l)).thenReturn(Optional.of(user));

        Users result = usersService.findById(1L);

        verify(usersRepository).findById(1L);
        assertThat(result).isEqualTo(user);
    }
    @Test
    void whenCallAdd_shouldCallUsersRepositoryAddAndReturnNewUsers() {
        Users user = new Users();
        user.setU_name("USERNAME");

        when(usersRepository.save(user)).thenReturn(user);

        Users result = usersService.add(user);

        verify(usersRepository).save(user);
        assertThat(result).isEqualTo(user);
    }
    @Test
    void whenCallUpdate_shouldCallUsersRepositoryUpdateAndReturnNewUser() {
        Users org_user = new Users();
        org_user.setU_name("USERNAME");
        org_user.setMail("mail_uno@mail.es");

        Users new_user = new Users();
        new_user.setMail("mail_dos@mail.es");

        Users upd_user = org_user;
        upd_user.setMail("mail_dos@mail.es");

        when(usersRepository.findById((long) 1)).thenReturn(Optional.of(org_user));
        when(usersRepository.save(any(Users.class))).thenReturn(upd_user);

        Users result = usersService.update(1, new_user);

        verify(usersRepository).findById((long) 1);
        verify(usersRepository).save(upd_user);
        assertThat(result).isEqualTo(upd_user);


    }
    //Comment
    @Test
    void whenCallDelete_shouldCallMessageRepositoryDeleteAndDeletePerm() {
        usersService.delete(1L);
        verify(usersRepository).deleteById(any(Long.class));
    }

}
