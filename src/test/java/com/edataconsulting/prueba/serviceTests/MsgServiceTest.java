package com.edataconsulting.prueba.serviceTests;

import com.edataconsulting.prueba.entity.Msg;
import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.repository.MsgRepository;
import com.edataconsulting.prueba.service.MsgService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;



@SpringBootTest
public class MsgServiceTest {
    private MsgService msgService;

    @MockBean
    private MsgRepository msgRepository;

    @BeforeEach
    void setUp() {
        msgService = new MsgService(msgRepository);
    }

    @Test
    void whenCallFindAll_shouldCallMsgRepositoryFindAllAndReturnResult() {
        List<Msg> msg_list = new ArrayList<>();
        Msg msg = new Msg();
        msg.setText("asd");
        msg_list.add(msg);


        when(msgRepository.findAll()).thenReturn(msg_list);

        List<Msg> result = msgService.findAll();

        verify(msgRepository).findAll();
        assertThat(result).isEqualTo(msg_list);
    }

    @Test
    void whenCallFindById_shouldCallMsgRepositoryFindByIdAndReturnMsg() {
        Msg msg = new Msg();
        msg.setText("asd");

        when(msgRepository.findById(1L)).thenReturn(Optional.of(msg));

        Msg result = msgService.findById(1L);

        verify(msgRepository).findById(1L);
        assertThat(result).isEqualTo(msg);
    }

    @Test
    void whenCallAdd_shouldCallMsgRepositoryAddAndReturnNewMsg() {
        Msg msg = new Msg();
        Users user = new Users();
        user.setU_name("USERNAME");
        msg.setUser(user);
        msg.setText("asd");

        when(msgRepository.save(msg)).thenReturn(msg);

        Msg result = msgService.add(msg);

        verify(msgRepository).save(msg);
        assertThat(result).isEqualTo(msg);
    }
    @Test
    void whenCallFindAllByUserId_shouldCallMsgRepositoryFindAllByUserIdAndReturnMsg() {
        Msg msg = new Msg();
        Users user = new Users();
        user.setU_name("USERNAME");
        msg.setUser(user);
        msg.setText("asd");
        List<Msg> msg_list = new ArrayList<>();
        msg_list.add(msg);

        when(msgRepository.findAllByUserId(1L)).thenReturn(msg_list);
        List<Msg> result = msgService.findAllByUserId(1L);

        verify(msgRepository).findAllByUserId(1L);
        assertThat(result).isEqualTo(msg_list);

    }

   @Test
    void whenCallUpdate_shouldCallMessageRepositoryUpdateAndReturnNewMessage() {
        Msg orig_msg = new Msg();
        Users user = new Users();
        user.setU_name("USERNAME");
        orig_msg.setUser(user);
        orig_msg.setText("asd");

        Msg new_msg = new Msg();
        new_msg.setText("jaja");

        Msg upd_msg = orig_msg;
        upd_msg.setText("jaja");

        when(msgRepository.findById((long) 1)).thenReturn(Optional.of(orig_msg));
        when(msgRepository.save(any(Msg.class))).thenReturn(upd_msg);

        Msg result = msgService.update(1, new_msg);

        verify(msgRepository).findById((long) 1);
        verify(msgRepository).save(upd_msg);
        assertThat(result).isEqualTo(upd_msg);
    }
    @Test
    void whenCallDelete_shouldCallMessageRepositoryDeleteAndDeletePerm() {
        msgService.delete(1L);
        verify(msgRepository).deleteById(any(Long.class));
    }
}
