package com.edataconsulting.prueba.contollerTests;

import com.edataconsulting.prueba.controller.MsgController;
import com.edataconsulting.prueba.entity.Msg;
import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.service.MsgService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class MsgControllerTest {
    private static final String MESSAGE_ENDPOINT = "/msg";
    @MockBean
    private MsgService msgService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Msg> msglist = new ArrayList<>();
        Msg msg = new Msg();
        msg.setText("asd");
        msglist.add(msg);

        when(msgService.findAll()).thenReturn(msglist);

        mockMvc.perform(MockMvcRequestBuilders.get(MESSAGE_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MsgController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].text").value("asd"));
    }
    @Test
    public void whenRequestFindById_thenReturnMessagesById() throws Exception{
        Msg msg = new Msg();
        Users users = new Users();
        users.setU_name("USER");
        msg.setUser(users);
        msg.setText("asd");

        when(msgService.findById(any(Long.class))).thenReturn(msg);
        mockMvc.perform(MockMvcRequestBuilders.get(MESSAGE_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MsgController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("asd"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.user.u_name").value("USER"));
    }
    @Test
    public void whenRequestAdd_thenReturnNewMessage() throws Exception{
        Msg msg = new Msg();
        Users user = new Users();
        user.setU_name("USER");
        msg.setUser(user);
        msg.setText("asd");

        String mock = "{\"text\": \"asd\", \"user\" : {\"name\": \"USER\"}}";

        when(msgService.add(any(Msg.class))).thenReturn(msg);
        mockMvc.perform(MockMvcRequestBuilders.post(MESSAGE_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MsgController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("asd"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.user.u_name").value("USER"));
    }
    @Test
    public void whenRequestUpdate_thenReturnUpdatedMessage() throws Exception{
        Msg msg = new Msg();
        Users user = new Users();
        user.setU_name("USER");
        msg.setUser(user);
        msg.setText("asd");

        String mock = "{\"text\": \"asd\", \"user\" : {\"name\": \"USER\"}}";

        when(msgService.update(any(Long.class),any(Msg.class))).thenReturn(msg);
        mockMvc.perform(MockMvcRequestBuilders.put(MESSAGE_ENDPOINT+"/1")
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MsgController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("update"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.text").value("asd"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.user.u_name").value("USER"));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(MESSAGE_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(MsgController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}