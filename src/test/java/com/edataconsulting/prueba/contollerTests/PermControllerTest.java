package com.edataconsulting.prueba.contollerTests;

import com.edataconsulting.prueba.controller.PermController;
import com.edataconsulting.prueba.entity.Perm;
import com.edataconsulting.prueba.service.PermService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
public class PermControllerTest {
    private static final String PERM_ENDPOINT = "/perm";
    @MockBean
    private PermService permService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Perm> perm_list = new ArrayList<>();
        Perm testperm = new Perm();
        testperm.setPerm_type("PERM1");
        perm_list.add(testperm);

        when(permService.findAll()).thenReturn(perm_list);

        mockMvc.perform(MockMvcRequestBuilders.get(PERM_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].perm_type").value("PERM1"));
    }

    @Test
    public void whenRequestFindById_thenReturnMessagesById() throws Exception {
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");

        when(permService.findById(any(Long.class))).thenReturn(perm);
        mockMvc.perform(MockMvcRequestBuilders.get(PERM_ENDPOINT + "/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.perm_type").value("PERM1"));
    }

    @Test
    public void whenRequestAdd_thenReturnNewMessage() throws Exception {
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");

        String mock = "{\"perm_type\": \"PERM1\"}";

        when(permService.add(any(Perm.class))).thenReturn(perm);
        mockMvc.perform(MockMvcRequestBuilders.post(PERM_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.perm_type").value("PERM1"));

    }

    @Test
    public void whenRequestUpdate_thenReturnUpdatedMessage() throws Exception {
        Perm perm = new Perm();
        perm.setPerm_type("PERM1");

        String mock = "{\"text\": \"PERM1\"}";

        when(permService.update(any(Long.class), any(Perm.class))).thenReturn(perm);
        mockMvc.perform(MockMvcRequestBuilders.put(PERM_ENDPOINT + "/1")
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("update"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.perm_type").value("PERM1"));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(PERM_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(PermController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

}
