package com.edataconsulting.prueba.contollerTests;

import com.edataconsulting.prueba.controller.UsersController;
import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.service.UsersService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@SpringBootTest
@AutoConfigureMockMvc
public class UsersControllerTest {
    private static final String USERS_ENDPOINT = "/users";
    @MockBean
    private UsersService usersService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void whenRequestFindAll_thenReturnAll() throws Exception {
        List<Users> user_list = new ArrayList<>();
        Users user = new Users();
        user.setId(1);
        user.setU_name("USERNAME");
        user_list.add(user);

        when(usersService.findAll()).thenReturn(user_list);

        mockMvc.perform(MockMvcRequestBuilders.get(USERS_ENDPOINT))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findAll"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].u_name").value("USERNAME"));
    }
    @Test
    public void whenRequestFindById_thenReturnMessagesById() throws Exception{
        Users user = new Users();
        user.setId(1);
        user.setU_name("USERNAME");
        user.setMail("david@mail.es");

        when(usersService.findById(any(Long.class))).thenReturn(user);
        mockMvc.perform(MockMvcRequestBuilders.get(USERS_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("findById"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.mail").value("david@mail.es"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.u_name").value("USERNAME"));

    }
    @Test
    public void whenRequestAdd_thenReturnNewMessage() throws Exception{
        Users uss = new Users();
        uss.setU_name("USERNAME");
        uss.setMail("david@mail.es");
        uss.setId(1);


        String mock = "{\"u_name\": \"USERNAME\"}";

        when(usersService.add(any(Users.class))).thenReturn(uss);
        mockMvc.perform(MockMvcRequestBuilders.post(USERS_ENDPOINT)
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("add"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.mail").value("david@mail.es"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.u_name").value("USERNAME"));
    }
    @Test
    public void whenRequestUpdate_thenReturnUpdatedMessage() throws Exception{
        Users uss = new Users();
        uss.setU_name("USERNAME");
        uss.setId(1);


        String mock = "{\"u_name\": \"USERNAME\"}";

        when(usersService.update(any(Long.class), any(Users.class))).thenReturn(uss);
        mockMvc.perform(MockMvcRequestBuilders.put(USERS_ENDPOINT + "/1")
                .content(mock).contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("update"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.u_name").value("USERNAME"));
    }
    @Test
    public void whenRequestDelete_thenCheckMethodIsCall() throws Exception{
        mockMvc.perform(MockMvcRequestBuilders.delete(USERS_ENDPOINT+"/1"))
                .andExpect(MockMvcResultMatchers.handler().handlerType(UsersController.class))
                .andExpect(MockMvcResultMatchers.handler().methodName("delete"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
