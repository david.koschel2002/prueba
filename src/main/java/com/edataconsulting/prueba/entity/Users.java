package com.edataconsulting.prueba.entity;

import java.time.LocalDate;
import java.util.List;
import javax.persistence.*;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String u_name;
    private String mail;
    private LocalDate date;
    private boolean connect = true;
    @ManyToMany
    @JoinTable(name = "permid_usersid",
            joinColumns = {@JoinColumn(name= "usersid")},
            inverseJoinColumns = {@JoinColumn(name = "permid")})
    List<Perm> perm;

    public Users() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getU_name() {
        return u_name;
    }

    public void setU_name(String u_name) {
        this.u_name = u_name;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public boolean isConnect() {
        return connect;
    }

    public void setConnect(boolean connect) {
        this.connect = connect;
    }

    public List<Perm> getPerm() {
        return perm;
    }

    public void setPerm(List<Perm> perm) {
        this.perm = perm;
    }
}
