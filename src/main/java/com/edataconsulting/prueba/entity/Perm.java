package com.edataconsulting.prueba.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Perm {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    private String perm_type;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPerm_type() {
        return perm_type;
    }

    public void setPerm_type(String perm_type) {
        this.perm_type = perm_type;
    }
    public Perm() {}

}
