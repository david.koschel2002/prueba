package com.edataconsulting.prueba.controller;

import com.edataconsulting.prueba.entity.Msg;
import com.edataconsulting.prueba.service.MsgService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RestController
@RequestMapping("msg")
@CrossOrigin


public class MsgController {

    private final MsgService msgService;

    public MsgController(MsgService msgService) {
        this.msgService = msgService;
    }

    @GetMapping("")
    public List<Msg> findAll() {
        return this.msgService.findAll();
    }

    @GetMapping("{id}")
    public Msg findById(@PathVariable long id)  {
        return this.msgService.findById(id);
    }

    @PostMapping()
    public Msg add(@RequestBody Msg msg1) {
        return this.msgService.add(msg1);
    }

    @PutMapping("{id}")
    public Msg update(@PathVariable long id, @RequestBody Msg msg) {
        return this.msgService.update(id, msg);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable long id) {
        this.msgService.delete(id);
    }
}
