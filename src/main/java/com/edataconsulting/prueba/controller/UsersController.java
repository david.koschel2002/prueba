package com.edataconsulting.prueba.controller;

import com.edataconsulting.prueba.entity.Msg;
import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.service.MsgService;
import com.edataconsulting.prueba.service.UsersService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@RestController
@RequestMapping("users")
@CrossOrigin

public class UsersController {

    private final UsersService usersService;
    private final MsgService msgService;

    public UsersController(UsersService usersService, MsgService msgService) {
        this.usersService = usersService;this.msgService = msgService;
    }

    @GetMapping("")
    public List<Users> findAll() {
        return this.usersService.findAll();
    }

    @GetMapping("{id}")
    public Users findById(@PathVariable long id)  {
        return this.usersService.findById(id);
    }

    @PostMapping()
    public Users add(@RequestBody Users user) {
        return this.usersService.add(user);
    }

    @PutMapping("{id}")
    public Users update(@PathVariable long id, @RequestBody Users user) {
        return this.usersService.update(id, user);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable long id) {
        this.usersService.delete(id);
    }

    @GetMapping("{id}/msg")
    public List<Msg> getUserMsg (@PathVariable long id){return msgService.findAllByUserId(id);}
}
