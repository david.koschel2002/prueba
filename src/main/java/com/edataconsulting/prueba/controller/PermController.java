package com.edataconsulting.prueba.controller;

import com.edataconsulting.prueba.entity.Perm;
import com.edataconsulting.prueba.service.PermService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;


@RestController
@RequestMapping("perm")
@CrossOrigin


public class PermController {

    private final PermService permService;

    public PermController(PermService permService) {
        this.permService = permService;
    }

    @GetMapping("")
    public List<Perm> findAll() {
        return this.permService.findAll();
    }

    @GetMapping("{id}")
    public Perm findById(@PathVariable long id)  {
        return this.permService.findById(id);
    }

    @PostMapping()
    public Perm add(@RequestBody Perm msg1) {
        return this.permService.add(msg1);
    }

    @PutMapping("{id}")
    public Perm update(@PathVariable long id, @RequestBody Perm perm) {
        return this.permService.update(id, perm);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable long id) {
        this.permService.delete(id);
    }
}
