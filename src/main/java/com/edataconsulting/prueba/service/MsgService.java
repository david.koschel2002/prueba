package com.edataconsulting.prueba.service;


import com.edataconsulting.prueba.entity.Msg;
import com.edataconsulting.prueba.repository.MsgRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service

public class MsgService {
    private MsgRepository msgRepository;

    public MsgService(MsgRepository msgRepository) {
        this.msgRepository = msgRepository;
    }

    public List<Msg> findAll() {
        return msgRepository.findAll();
    }

    public Msg findById(long id) {
        return msgRepository.findById(id).orElseThrow(EntityNotFoundException:: new);
    }

    public Msg add(Msg msg2) {
        return msgRepository.save(msg2);
    }

    public List<Msg> findAllByUserId(long id) {return msgRepository.findAllByUserId(id);}

    public Msg update(long id, Msg msg3) {
        Msg msgDB = msgRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        msgDB.setText(msg3.getText());
        msgDB.setDate(msg3.getDate());
        return msgRepository.save(msgDB);
    }

    public void delete(long id) {
        msgRepository.deleteById(id);
    }

}

