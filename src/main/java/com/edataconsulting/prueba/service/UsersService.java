package com.edataconsulting.prueba.service;

import com.edataconsulting.prueba.entity.Users;
import com.edataconsulting.prueba.repository.UsersRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {
    private UsersRepository usersRepository;

    public UsersService(UsersRepository usersRepository) {this.usersRepository = usersRepository;}

    public List<Users> findAll() {
        /*List<Users> lista = new ArrayList<>();
        Users user1 = new Users();
        user1.setMail("ool@sd");
        user1.setU_name("usuareio");
        user1.setConnect(false);
        user1.setId(1);*/
        return usersRepository.findAll();
    }

    public Users findById(long id) {return usersRepository.findById(id).orElseThrow(EntityNotFoundException::new);}

    public Users add (Users newuser) {
        newuser.setDate(LocalDate.now());
        return usersRepository.save(newuser);
    }

    public Users update(long id, Users user2) {
        Users userDB = usersRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        userDB.setU_name(user2.getU_name());
        userDB.setMail(user2.getMail());
        userDB.setPerm(user2.getPerm());
        userDB.setDate(user2.getDate());
        return usersRepository.save(userDB);
    }


    public void delete(long id) {
        Users userDB = usersRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        userDB.setConnect(false);
        usersRepository.save(userDB);
    }
}
