package com.edataconsulting.prueba.service;

import com.edataconsulting.prueba.entity.Perm;
import com.edataconsulting.prueba.repository.PermRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class PermService {
    private PermRepository permRepository;
    public PermService(PermRepository permRepository) {this.permRepository = permRepository;}
    public List<Perm> findAll() { return permRepository.findAll();}

    public Perm findById(long id) {return permRepository.findById(id).orElseThrow(EntityNotFoundException::new);}

    public Perm add(Perm any_perm) {return permRepository.save(any_perm);}

    public Perm update(long id, Perm perm1){
        Perm permDB = permRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        permDB.setPerm_type(perm1.getPerm_type());
        return permRepository.save(permDB);
    }
    public void delete(long id) {permRepository.deleteById(id);}
}
