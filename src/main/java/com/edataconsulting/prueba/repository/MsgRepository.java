package com.edataconsulting.prueba.repository;

import com.edataconsulting.prueba.entity.Msg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface MsgRepository extends JpaRepository<Msg, Long> {
    List<Msg> findAllByUserId (Long usersId);
}
