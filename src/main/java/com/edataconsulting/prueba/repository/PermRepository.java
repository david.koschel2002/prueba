package com.edataconsulting.prueba.repository;

import com.edataconsulting.prueba.entity.Perm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermRepository extends JpaRepository<Perm, Long> {}
