package com.edataconsulting.prueba.repository;

import com.edataconsulting.prueba.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, Long> {}
